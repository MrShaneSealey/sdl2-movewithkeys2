/*
  Moving the player with keys, using a timer.   
  - Second iteration:
    - Change velocity within the key event
    - Apply velocity in update 
*/

//For exit()
#include <stdlib.h>

//For printf()
#include <stdio.h>

//For round()
#include <math.h>

#if defined(_WIN32) || defined(_WIN64)
    //The SDL library
    #include "SDL.h"
    //Support for loading different types of images.
    #include "SDL_image.h"
#else
    #include "SDL2/SDL.h"
    #include "SDL2/SDL_image.h"
#endif

const int WINDOW_WIDTH = 800;
const int WINDOW_HEIGHT= 600;

const float FRAME_RATE = 60.0f;

const int SDL_OK = 0;

typedef Animation
{
    int maxFrames;
    int currentFrames;

    float frameTimeMax;
    float accumulator;

    SDL_Rect* frames;
};

typedef struct Player
{
    // Texture which stores the sprite sheet (this
    // will be optimised).
    SDL_Texture* playerTexture = nullptr;

    // Player properties
    SDL_Rect targetRectangle;
    float playerSpeed = 50.0f;
    float playerX = 250.0f;
    float playerY = 250.0f;
    float xVelocity = 0.0f;
    float yVelocity = 0.0f;

    // Sprite information
    const int KING_SPRITE_HEIGHT = 64;
    const int KING_SPRITE_WIDTH = 32;
    Animation* walkLeft;
};

//SDL_Texture* SDL_CreateTextureFromFile(const char* filename, SDL_Renderer *renderer);

void initAnimation(Animation* anim,
    int noOfFrames,
    const int SPRITE_WIDTH, const int SPRITE_HEIGHT,
    int row, int col);
void destAnimation(Animation* anim);

void initPlayer(Player* player, SDL_Renderer* renderer);
void destPlayer(Player* player);
void drawPlayer(Player* player, SDL_Renderer* renderer);
void processInput(Player* player, const Uint8* keyStates);
void updatePlayer(Player* player, float timeDeltaInSeconds);

int main( int argc, char* args[] )
{
    // Declare window and renderer objects
    SDL_Window*	     gameWindow = nullptr;
    SDL_Renderer*    gameRenderer = nullptr;

    //Variables
    SDL_Texture* backgroundTexture = nullptr;

    Player player;

    // Window control 
    SDL_Event event;
    bool quit = false;

    //keyboard
    const Uint8 *keyStates;

    // Timing variables
    unsigned int currentTimeIndex;
    unsigned int prevTimeIndex; 
    unsigned int timeDelta;
    float timeDeltaInSeconds;

    int sdl_status = SDL_Init(SDL_INIT_EVERYTHING);

    if(sdl_status != SDL_OK)
    {
        //SDL did not initialise, report and error and exit. 
        printf("Error -  SDL Initialisation Failed\n");
        exit(1);
    }

    gameWindow = SDL_CreateWindow("Hello CIS4008",   // Window title
                            SDL_WINDOWPOS_UNDEFINED, // X position
                            SDL_WINDOWPOS_UNDEFINED, // Y position
                            WINDOW_WIDTH,            // width
                            WINDOW_HEIGHT,           // height               
                            SDL_WINDOW_SHOWN);       // Window flags

   
    
    if(gameWindow != nullptr)
    {
        // if the window creation succeeded create our renderer
        gameRenderer = SDL_CreateRenderer(gameWindow, 0, 0);

        if(gameRenderer == nullptr)
        {
          printf("Error - SDL could not create renderer\n");
          exit(1);
        }
    }
    else
    {
        // could not create the window, so don't try and create the renderer. 
        printf("Error - SDL could not create Window\n");
        exit(1);
    }
    
    //track keystates array
    keyStates = SDL_GetKeyboardState(NULL);

    backgroundTexture = load_texture("assets/images/background.png", gameRenderer);

    initPlayer(&player, gameRenderer);

    prevTimeIndex = SDL_GetTicks();

    // Game loop
    while(!quit) // while quit is not true
    { 
        // Calculate time elapsed

        currentTimeIndex = SDL_GetTicks();	
        timeDelta = currentTimeIndex - prevTimeIndex; //time in milliseconds
        timeDeltaInSeconds = timeDelta * 0.001;
        	
        // Store current time index into prevTimeIndex for next frame
        prevTimeIndex = currentTimeIndex;
     	
       // Handle input 

        if (SDL_PollEvent(&event))  // test for events
        {
            switch (event.type)
            {
            case SDL_QUIT:
                quit = true;
                break;

                // Key pressed event
            case SDL_KEYDOWN:
                switch (event.key.keysym.sym)
                {
                case SDLK_ESCAPE:
                    quit = true;
                    break;
                }
                break;

                // Key released event
            case SDL_KEYUP:
                switch (event.key.keysym.sym)
                {
                case SDLK_ESCAPE:
                    //  Nothing to do here.
                    break;
                }
                break;

            default:
                // not an error, there's lots we don't handle. 
                break;
            }
        }

        processInput(&player, keyStates);

        updatePlayer(&player, timeDeltaInSeconds);

        // 1. Clear the screen
        SDL_SetRenderDrawColor(gameRenderer, 0, 0, 0, 255);
        // Colour provided as red, green, blue and alpha (transparency) values (ie. RGBA)

        SDL_RenderClear(gameRenderer);

        // 2. Draw the scene
        SDL_RenderCopy(gameRenderer, backgroundTexture, NULL, NULL);

        drawPlayer(&destPlayer, gameRenderer);

        // 3. Present the current frame to the screen
        SDL_RenderPresent(gameRenderer);
    }

    //Clean up!
    destPlayer(&player);
   
    SDL_DestroyTexture(backgroundTexture);
    backgroundTexture = nullptr;
    
    SDL_DestroyRenderer(gameRenderer);
    gameRenderer = nullptr;
    
    SDL_DestroyWindow(gameWindow);
    gameWindow = nullptr;   

    //Shutdown SDL - clear up resorces etc.
    SDL_Quit();

    return 0;
}

SDL_Texture* load_texture(const char* file, SDL_Renderer* GameRenderer)
{
    //Call Texture
    SDL_Surface* tmp = IMG_Load(file);

    if (!tmp)
    {
        printf("Could not load image: %s: %s\n", file, SDL_GetError());
    }

    SDL_Texture* texture = SDL_CreateTextureFromSurface(GameRenderer, tmp);

    if (!texture)
    {
        printf("Could not create texture from surface: %s: %s\n", file, SDL_GetError());
    }

    SDL_FreeSurface(tmp);
    tmp = nullptr;

    return texture;
}

void initAnimation(Animation* anim,
    int noOfFrames,
    const int SPRITE_WIDTH, const int SPRITE_HEIGHT,
    int row, int col)
{

    // set frame count.
    anim->maxFrames = noOfFrames;

    // allocate frame array
    anim->frames = new SDL_Rect[anim->maxFrames];

    //Setup animation frames - fixed row!
    for (int i = 0; i < anim->maxFrames; i++)
    {
        if (row == -1)
        {
            anim->frames[i].x = (i * SPRITE_WIDTH); //ith col.
            anim->frames[i].y = (col * SPRITE_HEIGHT); //col row. 
        }
        else
        {
            if (col == -1)
            {
                anim->frames[i].x = (row * SPRITE_WIDTH); //ith col.
                anim->frames[i].y = (i * SPRITE_HEIGHT); //col row. 
            }
            else
            {
                printf("Bad paramters to initAnimation!\n");
                //return an error?
            }

        }

        anim->frames[i].w = SPRITE_WIDTH;
        anim->frames[i].h = SPRITE_HEIGHT;
    }

    //set current animation frame to first frame. 
    anim->currentFrame = 0;

    //zero frame time accumulator
    anim->accumulator = 0.0f;
}

/**
 * destAnimation
 *
 * Function to clean up an animation structure.
 *
 * @param anim Animation structure to destroy
 */
void destAnimation(Animation* anim)
{
    //Free the memory - we allocated it with new
    // so must use the matching delete operation.

    delete anim->frames;

    // Good practice to set pointers to null after deleting
    // ths prevents accidental access. 
    anim->frames = nullptr;
}

void initPlayer(Player* player, SDL_Renderer* renderer)
{

    player->playerTexture = createTextureFromFile("assets/images/undeadking.png", renderer);

    // Allocate memory for the animation structure
    player->walkLeft = new Animation;

    // Setup the animation structure
    initAnimation(player->walkLeft, 3, player->KING_SPRITE_WIDTH, player->KING_SPRITE_HEIGHT, -1, 2);

    player->targetRectangle.w = player->KING_SPRITE_WIDTH;
    player->targetRectangle.h = player->KING_SPRITE_HEIGHT;
}

void destPlayer(Player* player)
{
    // Clean up animations - free memory
    destAnimation(player->walkLeft);

    // Clean up the animaton structure
    delete player->walkLeft;
    player->walkLeft = nullptr;

    // Clean up 
    SDL_DestroyTexture(player->playerTexture);
    player->playerTexture = nullptr;
}

void drawPlayer(Player* player, SDL_Renderer* renderer)
{
    // Get current animation (only have one)!
    Animation* current = player->walkLeft;

    SDL_RenderCopy(renderer, player->playerTexture, &current->frames[current->currentFrame], &player->targetRectangle);
}

void processInput(Player* player, const Uint8* keyStates)
{
    // Process Player Input

    //Input - keys/joysticks?
    float verticalInput = 0.0f;
    float horizontalInput = 0.0f;

    // input while the key is held down. 
    if (keyStates[SDL_SCANCODE_UP])
    {
        verticalInput -= 1.0f;
    }
    else
    {
        verticalInput += 0.0f;
    }

    // Calculate player velocity. 
    // Note: This is imperfect, no account taken of diagonal!
    player->xVelocity = verticalInput * player->playerSpeed;
    player->yVelocity = horizontalInput * player->playerSpeed;
}

void updatePlayer(Player* player, float timeDeltaInSeconds)
{
    // Calculate distance travelled since last update
    float yMovement = timeDeltaInSeconds * player->xVelocity;
    float xMovement = timeDeltaInSeconds * player->yVelocity;

    // Update player position.
    player->playerX += xMovement;
    player->playerY += yMovement;

    // Move sprite to nearest pixel location.
    player->targetRectangle.y = round(player->playerY);
    player->targetRectangle.x = round(player->playerX);

    // Store animation time delta
    player->walkLeft->accumulator += timeDeltaInSeconds;

    // Check if animation needs update
    if (player->walkLeft->accumulator > 0.4f) //25fps?
    {
        player->walkLeft->currentFrame++;
        player->walkLeft->accumulator = 0.0f;

        if (player->walkLeft->currentFrame >= player->walkLeft->maxFrames)
        {
            player->walkLeft->currentFrame = 0;
        }
    }
}